package com.example.exercise11

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.example.exercise11.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: Adapter
    private var filterList = mutableListOf<Item>()
    private val items = mutableListOf<Item>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        addItem()
        binding.recyclerView.layoutManager = GridLayoutManager(this, 3)
        adapter = Adapter()
        binding.recyclerView.adapter = adapter
        adapter.callback = { item, position ->
            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra("result", items[position])
            startActivity(intent)

        }

        adapter.setData(items)
        binding.searchView.isSubmitButtonEnabled = true


    }

    private fun addItem() {
        items.add(Item("Cat", R.drawable.cat, "This is a cat"))
        items.add(Item("Cat", R.drawable.cat, "This is a cat"))
        items.add(Item("Cat", R.drawable.cat, "This is a cat"))
        items.add(Item("Cat", R.drawable.cat, "This is a cat"))
        items.add(Item("Cat", R.drawable.cat, "This is a cat"))
        items.add(Item("Cat", R.drawable.cat, "This is a cat"))
        items.add(Item("Cat", R.drawable.cat, "This is a cat"))
        items.add(Item("Cat", R.drawable.cat, "This is a cat"))
        items.add(Item("Cat", R.drawable.cat, "This is a cat"))
        items.add(Item("Cat", R.drawable.cat, "This is a cat"))
        items.add(Item("Cat", R.drawable.cat, "This is a cat"))
        items.add(Item("Cat", R.drawable.cat, "This is a cat"))
        items.add(Item("Cat", R.drawable.cat, "This is a cat"))
        items.add(Item("Cat", R.drawable.cat, "This is a cat"))
        items.add(Item("Cat", R.drawable.cat, "This is a cat"))
        items.add(Item("Cat", R.drawable.cat, "This is a cat"))
        items.add(Item("Cat", R.drawable.cat, "This is a cat"))


    }

//    private fun search() {
//        binding.searchView.setOnQueryTextListener(object :SearchView.OnQueryTextListener{
//            override fun onQueryTextSubmit(arg: String?): Boolean {
//                if (titles.contains(arg)) {
//                    adapter.filter.filter(arg)
//                } else {
//                    Toast.makeText(this@MainActivity, "No match found", Toast.LENGTH_SHORT).show()
//                }
//                return false
//            }
//                override fun onQueryTextChange(arg: String?): Boolean {
//                adapter.filter.filter(arg)
//                return false
//            }
//        })
//    }




}