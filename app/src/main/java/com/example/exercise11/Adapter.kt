package com.example.exercise11

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.*
import com.example.exercise11.databinding.ItemLayoutBinding

typealias ClickItem = (item: Item, position: Int) -> Unit

class Adapter : RecyclerView.Adapter<Adapter.ViewHolder>() {


    private val items = mutableListOf<Item>()
    var callback: ClickItem? = null



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }


    override fun getItemCount() = items.size


    inner class ViewHolder(private val binding: ItemLayoutBinding) : RecyclerView.ViewHolder(binding.root),
        View.OnClickListener {
        private lateinit var model: Item

        fun onBind() {
            model = items[adapterPosition]
            binding.imageView.setImageResource(model.image)
            binding.root.setOnClickListener(this)


        }

        override fun onClick(v: View?) {
            callback?.invoke(model, adapterPosition)
        }


    }

    fun setData(mutableList: MutableList<Item>) {
        this.items.clear()
        this.items.addAll(mutableList)
        notifyDataSetChanged()
    }


}