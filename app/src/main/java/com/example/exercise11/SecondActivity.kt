package com.example.exercise11

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.exercise11.databinding.ActivitySecondBinding

class SecondActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySecondBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySecondBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init(){
        val result: Item? = intent.getParcelableExtra<Item>("result")
        if (result != null) {
            binding.image.setImageResource(result.image)
            binding.title.text = result.title
            binding.description.text = result.description
        }
    }
}