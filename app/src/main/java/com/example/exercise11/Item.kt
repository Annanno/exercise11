package com.example.exercise11

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Item(var title: String, val image: Int, val description: String) : Parcelable
